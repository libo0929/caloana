#!/usr/bin/env python

import sys

import numpy as np
from sklearn.neighbors import KDTree

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def plotMCP(vx, ep, plt):
    for i in range(0, len(vx)):
        x = [ vx[i][0], ep[i][0] ]
        y = [ vx[i][1], ep[i][1] ]
        z = [ vx[i][2], ep[i][2] ]

        plt.plot(x, y, z, linestyle='dotted', color='g')

#########################################
def getEventEnergy(evt):
    hitPosEn = evt['hitPosEn']

    X = []
    Y = []
    Z = []
    E = []

    for hpe in hitPosEn:

        if hpe[3] < 0.0:
            continue
       
        #print('---', hpe[0], hpe[1], hpe[2], hpe[3])
        X.append(hpe[0]) # x
        Y.append(hpe[1]) # y
        Z.append(hpe[2]) # z
        E.append(hpe[3]) # calibrated hit energy
                         # hpe[4] : layer

        hitCont = hpe[5]

    xArr = np.array(X)
    yArr = np.array(Y)
    zArr = np.array(Z)
    eArr = np.array(E)

    return np.sum(eArr)
            
    #----------------hit density---------------------------



if __name__=='__main__':
        if len(sys.argv) == 2:
            fileName = sys.argv[1]
	
        if len(sys.argv) == 1:
            fileName = "ecal_pi0_50GeV.npz"

        A = np.load(fileName, allow_pickle=True)
        evts = A['CaloEvts']

        evtsEnergy = []
        
        for evt in evts:
            evtEn = getEventEnergy(evt)
            evtsEnergy.append(evtEn)
            #print('evt energy:', evtEn)

        plt.hist(evtsEnergy, 20)
        plt.show()
