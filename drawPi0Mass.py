#!/usr/bin/env python

import sys

import numpy as np
from sklearn.neighbors import KDTree

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def getMass(evt):
    #----------------mcp---------------------------
    mcps = evt['mcp']

    momentum = []

    if len(mcps)!=2:
        print('#MCP is not 2, return')
        return -1

    mcpEnergy = []

    for mcp in mcps.values():
        #vx.append( [mcp[2], mcp[3], mcp[4]] )
        #ep.append( [mcp[5], mcp[6], mcp[7]] )

        if mcp[0]!=22:
            print('particle is not photon')
            return -1

        p0 = mcp[1] # photon energy

        px = mcp[5] - mcp[2] 
        py = mcp[6] - mcp[3]
        pz = mcp[7] - mcp[4]

        p = np.sqrt(px*px + py*py + pz*pz)
        px = px/p * p0
        py = py/p * p0
        pz = pz/p * p0

        mcpEnergy.append(mcp[1])
        momentum.append( [px, py, pz] )

        #print('PDG ', mcp[0], ', E: ', mcp[1], ', vx: ', mcp[2], mcp[3], mcp[4], 'ep: ', mcp[5], mcp[6], mcp[7])
        #print('PDG ', mcp[0], ', E: ', mcp[1])

    #print(mcpEnergy[0], mcpEnergy[1], momentum[0][0], momentum[0][1], momentum[0][2])

    e12 = mcpEnergy[0] + mcpEnergy[1]
    p12x = momentum[0][0] + momentum[1][0]
    p12y = momentum[0][1] + momentum[1][1]
    p12z = momentum[0][2] + momentum[1][2]

    mPi0 = e12*e12 - (p12x*p12x + p12y*p12y + p12z*p12z)
    mPi0 = np.sqrt(mPi0)

    #print(mPi0)

    return mPi0


if __name__=='__main__':
        if len(sys.argv) == 2:
            fileName = sys.argv[1]
	
        if len(sys.argv) == 1:
            fileName = "ecal_pi0_50GeV.npz"

        A = np.load(fileName, allow_pickle=True)
        evts = A['CaloEvts']
        
        pi0Mass = []

        for evt in evts:
            print('Event: ', evt['evtNum'])
            mass = getMass(evt)

            if mass <= 0:
                continue
                
            pi0Mass.append(mass)
            print(mass)

        plt.hist(pi0Mass, 20, [0, 0.3])
        plt.show()
