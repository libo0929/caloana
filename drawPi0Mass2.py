#!/usr/bin/env python

import sys

import numpy as np
from sklearn.neighbors import KDTree

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def getMass(evt):
    #----------------mcp---------------------------
    mcps = evt['mcp']

    if len(mcps)!=2:
        print('#MCP is not 2, return')
        return -200

    #momentum = []
    #mcpEnergy = []

    #for mcp in mcps.values():
    #    #vx.append( [mcp[2], mcp[3], mcp[4]] )
    #    #ep.append( [mcp[5], mcp[6], mcp[7]] )

    #    if mcp[0]!=22:
    #        print('particle is not photon')
    #        return -1

    #    p0 = mcp[1] # photon energy

    #    px = mcp[5] - mcp[2] 
    #    py = mcp[6] - mcp[3]
    #    pz = mcp[7] - mcp[4]

    #    p = np.sqrt(px*px + py*py + pz*pz)
    #    px = px/p * p0
    #    py = py/p * p0
    #    pz = pz/p * p0

    #    mcpEnergy.append(p0)
    #    momentum.append( [px, py, pz] )


    #    #print('PDG ', mcp[0], ', E: ', mcp[1], ', vx: ', mcp[2], mcp[3], mcp[4], 'ep: ', mcp[5], mcp[6], mcp[7])
    #    #print('PDG ', mcp[0], ', E: ', mcp[1])

    ##print(mcpEnergy[0], mcpEnergy[1], momentum[0][0], momentum[0][1], momentum[0][2])

    #e12 = mcpEnergy[0] + mcpEnergy[1]
    #p12x = momentum[0][0] + momentum[1][0]
    #p12y = momentum[0][1] + momentum[1][1]
    #p12z = momentum[0][2] + momentum[1][2]

    #mPi0 = e12*e12 - (p12x*p12x + p12y*p12y + p12z*p12z)
    #mPi0 = np.sqrt(mPi0)

    #print(mPi0)
    #return mPi0
    ##########################################
    hitPosEn = evt['hitPosEn']

    clusters = {}

    for hpe in hitPosEn:
        hitX = hpe[0] # x
        hitY = hpe[1] # y
        hitZ = hpe[2] # z
        hitEnergy = hpe[3]  # the calibrated hit energy

        if hitEnergy < 0.0:
            continue
       
        hitCont = hpe[5]

        # calc total deposited energy 
        hitDepEn = 0.

        for mcpe, ehit in hitCont.items():
            hitDepEn += ehit

        # create hits according to the energy ratio
        for mcpe, ehit in hitCont.items():
            hitEnergyRatio = ehit/hitDepEn 
            hitE = hitEnergyRatio * hitEnergy

            # if the shower from the mcp energy does not exsit, create the shower
            if not mcpe in clusters.keys():
                clusters[mcpe] = []

            clusters[mcpe].append( [hitX, hitY, hitZ, hitE] )

    clustersPE = []

    for mcpe, hitsList in clusters.items(): 
        cluX = 0
        cluY = 0
        cluZ = 0
        cluE = 0

        for hit in hitsList:
            hitE = hit[3]

            cluX += hit[0] * hitE
            cluY += hit[1] * hitE
            cluZ += hit[2] * hitE
            cluE += hitE

        p0 = cluE # photon energy

        px = cluX/cluE 
        py = cluY/cluE
        pz = cluZ/cluE

        p = np.sqrt(px*px + py*py + pz*pz)
        px = px/p * p0
        py = py/p * p0
        pz = pz/p * p0

        clustersPE.append( [p0, px, py, pz] )

    if len(clustersPE) != 2:
        print('error!!!!')
        return -100;

    e12 = clustersPE[0][0] + clustersPE[1][0]
    p12x = clustersPE[0][1] + clustersPE[1][1]
    p12y = clustersPE[0][2] + clustersPE[1][2]
    p12z = clustersPE[0][3] + clustersPE[1][3]

    mPi0 = e12*e12 - (p12x*p12x + p12y*p12y + p12z*p12z)
    mPi0 = np.sqrt(mPi0)

    return mPi0


if __name__=='__main__':
        if len(sys.argv) == 2:
            fileName = sys.argv[1]
	
        if len(sys.argv) == 1:
            fileName = "ecal_pi0_50GeV.npz"

        A = np.load(fileName, allow_pickle=True)
        evts = A['CaloEvts']
        
        pi0Mass = []

        for evt in evts:
            print('Event: ', evt['evtNum'])
            mass = getMass(evt)

            if mass > 0:
                pi0Mass.append(mass)

        plt.hist(pi0Mass, 50, [0, 0.3])
        plt.show()
