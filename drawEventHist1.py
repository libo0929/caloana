#!/usr/bin/env python

import sys

import math

import numpy as np
from sklearn.neighbors import KDTree

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def plotMCP(vx, ep, plt):
    for i in range(0, len(vx)):
        x = [ vx[i][0], ep[i][0] ]
        y = [ vx[i][1], ep[i][1] ]
        z = [ vx[i][2], ep[i][2] ]

        plt.plot(x, y, z, linestyle='dotted', color='g')


#########################################
def drawEvent(evt):
    fig = plt.figure(figsize=(18, 9))

    hitPosEn = evt['hitPosEn']

    X = []
    Y = []
    Z = []
    E = []

    allHitPos = []

    for hpe in hitPosEn:
        if hpe[3] < 0.0:
            continue
       
        #print('---', hpe[0], hpe[1], hpe[2])
        X.append(hpe[0])
        Y.append(hpe[1])
        Z.append(hpe[2])
        E.append(hpe[3])

        #hitCont = hpe[5]

        #for mcpe, ehit in hitCont.items():
        #    print('     --->', mcpe, ehit)

        allHitPos.append([hpe[0], hpe[1], hpe[2]])

    xArr = np.array(X)
    yArr = np.array(Y)
    zArr = np.array(Z)
    eArr = np.array(E)

    caloHitsArray = np.array(allHitPos)
    calHitsTree = KDTree(caloHitsArray)
    #hitsDensity = calHitsTree.kernel_density(caloHitsArray[:], h=10, kernel='cosine')
    neighborHits = calHitsTree.query_radius(caloHitsArray[:], r=10.)

    print('# of hits: ', len(caloHitsArray))

    #----------------mcp---------------------------
    mcps = evt['mcp']

    vx = [] # vertex
    ep = [] # endpoint

    for mcp in mcps.values():
            vx.append( [mcp[2], mcp[3], mcp[4]] )
            ep.append( [mcp[5], mcp[6], mcp[7]] )
            print('PDG ', mcp[0], ', E: ', mcp[1])

    #----------------hit density---------------------------
    energyDensities = []

    for i in range(0, len(caloHitsArray)):
        neighbors = neighborHits[i]

        energyDensity = 0.

        for neighbor in neighbors:
            p0 = np.array([xArr[i], yArr[i], zArr[i]])
            p1 = np.array([xArr[neighbor], yArr[neighbor], zArr[neighbor]])
            energyDensity += eArr[neighbor] * math.exp( -np.linalg.norm(p1 - p0) )
            #energyDensity += eArr[neighbor]

        energyDensities.append(energyDensity)

        #print('------------')
    
    energyDensitiesArray = np.array(energyDensities)   

    #plt.hist(eArr, log=True)
    #plt.hist(neighborHits)
    plt.hist(energyDensitiesArray, log=True, bins=100)
    
    fig = plt.gcf()
    fig.savefig('fig.pdf', format='pdf', dpi=1000)

    plt.show()


if __name__=='__main__':
    if len(sys.argv) == 2:
            fileName = sys.argv[1]
	
    if len(sys.argv) == 1:
        fileName = "ecal_pi0_50GeV.npz"

    A = np.load(fileName, allow_pickle=True)
    evts = A['CaloEvts']
    
    canPrint = False 

    text = ''

    for evt in evts:
        print('Event: ', evt['evtNum'])

        text = input('press enter to draw event, s to skip, e/q to exit ')

        if text != 'e' and text != 's' and text != 'q':
            drawEvent(evt)

        print('-------------------------------------------------------')
    
        if text == 'e' or text == 'q':
            print('Exit ...')
            break
