TH1F* plotDistHist(double piE) // GeV
{
	const double pi0Ek = piE;     // 10.0; // GeV
	const double pi0Mass = 0.135; // GeV
	const double totalEnergy = pi0Mass + pi0Ek;
	const double gammaFactor = totalEnergy / pi0Mass;
	const double betaFactor  = sqrt(1. - 1./gammaFactor/gammaFactor);

	const double ecal_a0 = 0.17;
	const double ecal_a1 = 0.01;
		
	const double detR = 1800.;

	ROOT::Math::XYZTVector pi0Vec;
	ROOT::Math::XYZTVector gmVec1;
	ROOT::Math::XYZTVector gmVec2;

	//std::cout << setprecision(10) << "beta: " << betaFactor << std::endl;
	ROOT::Math::BoostZ boostz(betaFactor);

    string name = "dxy" + std::to_string(piE);

	//TH1F* mass = new TH1F("hist", "#pi^{0} invariant mass", 100, 0.02, 0.23);
	TH1F* histDxy = new TH1F(name.c_str(), name.c_str(), 2000, 0, 200);
	//TH1F* histDxy = new TH1F("dxy", "dxy", 100, 5, 13);
	//TH1F* histAngle = new TH1F("Angle", "angle", 100, -2., 2.);

	for(unsigned int i = 0; i < 100000; ++i)
	{
		//std::cout << "Event: " << i << std::endl;

		// photon direction 
		double costh = gRandom->Uniform(-1, 1);

	    double phi   = gRandom->Uniform(0., 2. * TMath::Pi());
	    double sinth = sqrt(1-costh*costh);

		// photon energy is half that of pi0
		double E  = pi0Mass/2.;

	    double pX = E * sinth * cos(phi);
	    double pY = E * sinth * sin(phi);
	    double pZ = E * costh;


		gmVec1.SetPxPyPzE(pX, pY, pZ, E);
		gmVec2.SetPxPyPzE(-pX, -pY, -pZ, E);

		pi0Vec = boostz(pi0Vec);
		gmVec1 = boostz(gmVec1);
		gmVec2 = boostz(gmVec2);

		if(gmVec1.Z() < 0. || gmVec2.Z() < 0.) continue;

		ROOT::Math::XYZTVector vecSum = gmVec1 + gmVec2;

		// ECAL
		TVector3 gmVec1_ecal_dir;
		TVector3 gmVec2_ecal_dir;

		gmVec1_ecal_dir.SetXYZ( gmVec1.X(), gmVec1.Y(), gmVec1.Z() );
		gmVec2_ecal_dir.SetXYZ( gmVec2.X(), gmVec2.Y(), gmVec2.Z() );

		gmVec1_ecal_dir = gmVec1_ecal_dir.Unit();
		gmVec2_ecal_dir = gmVec2_ecal_dir.Unit();
		
	    ROOT::Math::XYZTVector gmVec1_ecal;
	    ROOT::Math::XYZTVector gmVec2_ecal;

		double gm1E = gmVec1.E();
		double gm2E = gmVec2.E();

		// shower energy smearing
		gm1E = gRandom->Gaus( gm1E, ecal_a0 * sqrt(gm1E) + ecal_a1 * gm1E );
		gm2E = gRandom->Gaus( gm2E, ecal_a0 * sqrt(gm2E) + ecal_a1 * gm2E );

		// set new shower energy 
		gmVec1_ecal.SetPxPyPzE(gm1E * gmVec1_ecal_dir.X(), gm1E * gmVec1_ecal_dir.Y(), gm1E * gmVec1_ecal_dir.Z(), gm1E);
		gmVec2_ecal.SetPxPyPzE(gm2E * gmVec2_ecal_dir.X(), gm2E * gmVec2_ecal_dir.Y(), gm2E * gmVec2_ecal_dir.Z(), gm2E);

		ROOT::Math::XYZTVector vecSum_ecal = gmVec1_ecal + gmVec2_ecal;


		double dzOverPz1 = detR/gmVec1_ecal_dir.Z();
		double x1 = gmVec1_ecal_dir.X() * dzOverPz1;
		double y1 = gmVec1_ecal_dir.Y() * dzOverPz1;

		double dzOverPz2 = detR/gmVec2_ecal_dir.Z();
		double x2 = gmVec2_ecal_dir.X() * dzOverPz2;
		double y2 = gmVec2_ecal_dir.Y() * dzOverPz2;

		//std::cout << "E1, E2: " << gm1E << ", " << gm2E << ", invariant mass (ECAL): " << vecSum_ecal.M() << std::endl;
		//std::cout << "(x,y)_1: " << x1 << ", " << y1 << ", (x,y)_2: " << x2 << ", " << y2 << std::endl;
		double dXY = TMath::Sqrt( (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) );

		//mass->Fill( vecSum_ecal.M() );
		histDxy->Fill( dXY );
		//histAngle->Fill(sinth);
		//histAngle->Fill(costh);
	}

	//mass->Draw();
	//histDxy->Draw();
	//histAngle->Draw();
	//
	return histDxy;
}

int pi0Dist()
{
	gStyle->SetOptStat(0);

	TH1F* hist10 = plotDistHist(10);
	hist10->SetTitle("");
	hist10->SetLineColor(kRed);
	hist10->SetLineStyle(1);
	hist10->GetXaxis()->SetTitle("distance of incident di-photon (mm)");
	
	TH1F* hist20 = plotDistHist(20);
	hist20->SetLineColor(kRed);

	TH1F* hist30 = plotDistHist(30);
	hist30->SetLineColor(kBlue);

	TH1F* hist40 = plotDistHist(40);
	hist40->SetLineColor(kBlue);

	TH1F* hist50 = plotDistHist(50);
	hist50->SetLineColor(kGreen);

	TH1F* hist60 = plotDistHist(60);
	hist60->SetLineColor(kGreen);

	TH1F* hist70 = plotDistHist(70);
	hist70->SetLineColor(kOrange);

	TH1F* hist80 = plotDistHist(80);
	hist80->SetLineColor(kOrange);

#if 1
	hist10->Scale(1./hist10->Integral());
	hist20->Scale(1./hist20->Integral());
	hist30->Scale(1./hist30->Integral());
	hist40->Scale(1./hist40->Integral());
	hist50->Scale(1./hist50->Integral());
	hist60->Scale(1./hist60->Integral());
	hist70->Scale(1./hist70->Integral());
	hist80->Scale(1./hist80->Integral());
#endif

	hist10->GetYaxis()->SetRangeUser(0, 0.15);
	hist10->GetXaxis()->SetRangeUser(0, 60);
	hist10->Draw("hist");
	//hist20->Draw("hist&&same");
	hist30->Draw("hist&&same");
	//hist40->Draw("hist&&same");
	hist50->Draw("hist&&same");
	//hist60->Draw("hist&&same");
	hist70->Draw("hist&&same");
	//hist80->Draw("hist&&same");

	return 0;
}
